#pragma once

#include <Object.h>

class Terrain : public Object {
public:
    Terrain() : Terrain(256) {};
    Terrain(GLuint vxCount);
    Terrain(GLfloat x, GLfloat y, GLfloat z) : Object(x, y, z) {};
};