#pragma once

#include <GL/glew.h>
#include "Object.h"

class Pane : public Object {
public:
    Pane() : Pane(0, 0, 0){};
    Pane(GLfloat x, GLfloat y, GLfloat z);
};