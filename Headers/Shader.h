#pragma once

#include <string>
#include <GL/glew.h>

class Shader {
    GLuint fragmentID;
    GLuint vertexID;
    GLuint programID;

    void printError();
    void create();
    void link();
    void validate();

    GLuint createShader(const GLchar *code, GLenum shaderType);
    
public:
    Shader(const std::string, const std::string);
    ~Shader();

    GLuint getProgramID();

    void use();
};