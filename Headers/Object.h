#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <Base.h>
#include <deque>
#include <Material.h>

class Object : public Base {
    void draw();

protected:
    glm::vec3 worldLocation;
    glm::vec3 scale;
    
    GLfloat rotation = 90.f;
	GLfloat rotx = 1.f;
	GLfloat roty = 1.f;
	GLfloat rotz = 1.f;

    GLuint VBO;
    GLuint IBO;
    GLuint VAO;
    GLuint CBO;
    GLuint NBO;
    
    Material *mat;
    
    GLuint indsCount;
    bool isVisible = true;

    Object() : Object(0, 0, 0) {};
    Object(GLfloat x, GLfloat y, GLfloat z) : worldLocation(x, y, z), scale(1, 1, 1) {};

public:
    void render();
    void setWorldLocation(glm::vec3 loc);
    void setWorldLocation(GLfloat x, GLfloat y, GLfloat z);
    void setScale(glm::vec3 scale);
    void setScale(GLfloat x, GLfloat y, GLfloat z);
    void setVisible(bool b);
    void setRotation(GLfloat deg, GLfloat x, GLfloat y, GLfloat z);
    void setMaterial(Material *m);

    glm::vec3 getWorldLocation();
    glm::vec3 getScale();
    Material* getMaterial();

    virtual void tick();
};