#pragma once

class Texture {
public:
    int width;
    int height;
    int ch;
    unsigned char *data;

    Texture(const char* path);
    ~Texture();
};