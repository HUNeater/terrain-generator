#pragma once

#include <GLFW/glfw3.h>
#include <iostream>
#include <deque>
#include <Scene.h>

class Scene;

class Engine {
    GLFWwindow *window;
    Scene *activeScene;

    int width = 640;
    int height = 480;

    const char *TITLE = "Engine Prototype";
    int initGl();
    int initWindow();
    int initGlfw();
    int initGlew();
    void static errorHandler(int, const char *);
    void static resizeHandler(GLFWwindow *, int, int);
    void loop();
    void tick(double delta);
    void render();
    void initEngine();

public:
    Material *defaultMaterial;
    Shader *shader;

    int start();
};