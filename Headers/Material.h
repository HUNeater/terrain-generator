#pragma once

#include <Texture.h>
#include <Base.h>

class Material : public Base {
    Shader *shader;
    GLuint texture;
    GLfloat textureScale = 1.0f;

public:
    Material(Shader *s);
    ~Material();

    void use();
    void setShader(Shader *s);
    void setTexture(Texture *tex);
    void setTextureScale(GLfloat scale);

    Shader* getShader();
};