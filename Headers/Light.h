#pragma once

#include <Object.h>

class Light : public Object {
    virtual void tick() override;

protected:
    glm::vec3 lightColor = glm::vec3(1, 1, 1);

public:
    Light();
};