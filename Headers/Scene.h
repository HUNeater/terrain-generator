#pragma once

#include <deque>
#include <Object.h>
#include <Camera.h>
#include <Engine.h>

class Engine;

class Scene {
    Engine *engine;
    Camera *camera;
    Object *light;
    std::deque<Object *> objects;

public:
    Scene(Engine *e);

    void render();
    void tick(double delta);
    void resize(int width, int height);

    template<typename Type>
    Object *createObject() {
        Object *obj = new Type();
        obj->setMaterial(engine->defaultMaterial);

        objects.push_back(obj);
        return obj;
    }
};