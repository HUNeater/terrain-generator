#pragma once

#include <glm/glm.hpp>
#include <Shader.h>

class Base {
protected:
    void uniform(Shader *shader, const GLchar var[], glm::mat4 &val);

    void uniform(Shader *shader, const GLchar var[], glm::vec3 &val);

    void uniform(Shader &shader, const GLchar var[], glm::vec2 val);

    void uniform(Shader &shader, const GLchar var[], GLfloat val);

    void uniform(Shader &shader, const GLchar var[], GLint val);

    void uniform(Shader &shader, const GLchar var[], GLfloat x, GLfloat y);

    void uniform(Shader *shader, const GLchar var[], GLfloat &x, GLfloat &y, GLfloat &z);
};