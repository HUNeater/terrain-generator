#pragma once

#include "Object.h"

class Camera : public Object {
    glm::vec3 target;
    std::deque<Shader*> shaders;

public:
    Camera() : Camera(4, 3, 3) {}
    Camera(GLfloat x, GLfloat y, GLfloat z);

    void updateProjection(int width, int height);
    void addShader(Shader *s);

    Camera* setTarget(glm::vec3 target);
    Camera* setTarget(GLfloat x, GLfloat y, GLfloat z);

    virtual void tick() override;
};