#include <Object.h>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

void Object::render() {
    tick();

    if (isVisible) {
        glBindVertexArray(VAO);
        draw();
    }
}

void Object::draw() {
    mat->use();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDrawElements(GL_TRIANGLES, indsCount, GL_UNSIGNED_INT, 0);
}

void Object::tick() {
    glm::mat4 modelMatrix = glm::mat4();
    modelMatrix = glm::translate(modelMatrix, worldLocation);
    modelMatrix = glm::rotate(modelMatrix, rotation, glm::vec3(rotx, roty, rotz));
    modelMatrix = glm::scale(modelMatrix, scale);

    uniform(mat->getShader(), "model", modelMatrix);
}

void Object::setScale(glm::vec3 scale) {
    this->scale = scale;
}

void Object::setMaterial(Material *m) {
    mat = m;
}

void Object::setRotation(GLfloat deg, GLfloat x, GLfloat y, GLfloat z) {
    rotation = glm::radians(deg);
    rotx = x;
    roty = y;
    rotz = z;
}

void Object::setVisible(bool b) {
    isVisible = b;
}

void Object::setScale(GLfloat x, GLfloat y, GLfloat z) {
    scale.x = x;
    scale.y = y;
    scale.z = z;
} 

void Object::setWorldLocation(glm::vec3 loc) {
    worldLocation = loc;
}

void Object::setWorldLocation(GLfloat x, GLfloat y, GLfloat z) {
    worldLocation.x = x;
    worldLocation.y = y;
    worldLocation.z = z;
}

glm::vec3 Object::getWorldLocation() {
    return worldLocation;
};

glm::vec3 Object::getScale() {
    return scale;
}

Material* Object::getMaterial() {
    return mat;
}