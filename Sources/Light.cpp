#include <Light.h>
#include <iostream>
#include <Noise.h>

Light::Light() {
    isVisible = false;

    int size = 2;
    siv::PerlinNoise perlin(rand());

    // 0.1 - 64
    double frequency = 4;

    // 1 - 16
	int octaves = 16;

    const double fx = size / frequency;
    const double fy = size / frequency;

    GLfloat verts[size * size * 3]; 
    GLuint i = 0;
    for (GLuint x = 0; x < size; ++x) {
        for (GLuint y = 0; y < size; ++y) {
            verts[i++] = (GLfloat) x / (size - 1);
            verts[i++] = (GLfloat) y / (size - 1);
            verts[i++] = (GLfloat) perlin.octaveNoise0_1(y / fy, x / fx, octaves);
        }
    }
    
    indsCount = (size - 1) * (size - 1) * 6;
    GLuint inds[indsCount];
    i = 0;    
    for (GLuint x = 0; x < size * (size - 1); x += size) {
        for (GLuint y = x; y < x + size - 1; ++y) {
            inds[i++] = y;
            inds[i++] = y + 1;
            inds[i++] = y + 1 + size;

            inds[i++] = y;
            inds[i++] = y + size;
            inds[i++] = y + size + 1;
        }
    }

    i = 0;
    GLfloat colors[size * size * 3];
    for (GLuint x = 0; x < size; ++x) {
        for (GLuint y = 0; y < size; ++y) {
            /*
            colors[i++] = 0.7 - verts[i];
            colors[i++] = verts[i];
            colors[i++] = verts[i];
            */
            
            colors[i++] = 1.0;
            colors[i++] = 1.0;
            colors[i++] = 1.0;            
            
        }
    }

    std::cout << perlin.octaveNoise0_1(1 / fx, 1 / fy, octaves) << std::endl;
    std::cout << perlin.octaveNoise0_1(1 / fx, 1 / fy, octaves) << std::endl;

    i = 0;
    GLfloat normals[size * size * 3];
    for (GLuint x = 0; x < size; ++x) {
        for (GLuint y = 0; y < size; ++y) {
            float off = 0.2f;

            float hL = perlin.octaveNoise0_1((y - off) / fy, x / fx, octaves);
            float hR = perlin.octaveNoise0_1((y + off) / fy, x / fx, octaves);
            float hD = perlin.octaveNoise0_1(y / fy, (x - off) / fx, octaves);
            float hU = perlin.octaveNoise0_1(y / fy, (x + off) / fx, octaves);
            
            glm::vec3 n;
            n.x = hL - hR;
            n.y = hD - hU;
            n.z = 2.0;

            n = glm::normalize(n);

            normals[i++] = n.x;
            normals[i++] = n.z;
            normals[i++] = n.y;
        }
    }

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    // indicies
    glGenBuffers(1, &IBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(inds), inds, GL_STATIC_DRAW);

    // vertices
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid *) 0);
    glEnableVertexAttribArray(0);

    // colors
    glGenBuffers(1, &CBO);
    glBindBuffer(GL_ARRAY_BUFFER, CBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid *) 0);
    glEnableVertexAttribArray(1);

    // normals
    glGenBuffers(1, &NBO);
    glBindBuffer(GL_ARRAY_BUFFER, NBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid *) 0);    
    glEnableVertexAttribArray(2);
    
    glBindVertexArray(0);
}

void Light::tick() {
    Object::tick();

    uniform(mat->getShader(), "lightPosition", worldLocation);
    uniform(mat->getShader(), "lightColor", lightColor);
}