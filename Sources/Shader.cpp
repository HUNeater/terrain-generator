#include <Shader.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

Shader::Shader(const std::string vxCode, const std::string frCode) {
    create();
    vertexID = createShader(vxCode.c_str(), GL_VERTEX_SHADER);
    fragmentID = createShader(frCode.c_str(), GL_FRAGMENT_SHADER);
    link();
    validate();
}

void Shader::create() {
    programID = glCreateProgram();
    if (programID == 0) {
        printError();
    }
}

void Shader::link() {
    glLinkProgram(programID);
    GLint linked;
    glGetProgramiv(programID, GL_LINK_STATUS, &linked);
    if (linked == GL_FALSE) {
        printError();
    }
}

void Shader::validate() {
    glValidateProgram(programID);
    GLint validated;
    glGetProgramiv(programID, GL_VALIDATE_STATUS, &validated);
    if (validated == GL_FALSE) {
        printError();
    }
}

GLuint Shader::createShader(const GLchar *code, GLenum shaderType) {
    auto shaderId = glCreateShader(shaderType);
    if (shaderId == 0) {
        printError();
    }

    // compiling
    glShaderSource(shaderId, 1, &code, NULL);
    glCompileShader(shaderId);
    GLint compiled;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compiled);
    if (compiled == GL_FALSE) {
        printError();
    }

    glAttachShader(programID, shaderId);

    return shaderId;
}

void Shader::printError() {
    GLint maxLength;
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &maxLength);

    std::vector<GLchar> infoLog((unsigned long) maxLength);
    glGetProgramInfoLog(programID, maxLength, &maxLength, &infoLog[0]);

    for (auto &&e : infoLog) {
        std::cerr << e;
    }
}

void Shader::use() {
    glUseProgram(programID);
}

GLuint Shader::getProgramID() {
    return programID;
}

Shader::~Shader() {
    glDetachShader(programID, vertexID);
    glDetachShader(programID, fragmentID);
    glDeleteProgram(programID);
}