#include <Texture.h>
#include <stb_image.h>

Texture::Texture(const char *path) {
    data = stbi_load(path, &width, &height, &ch, 0);
}

Texture::~Texture() {
    stbi_image_free(data);
}