#include <Scene.h>
#include <Player.h>
#include <Terrain.h>
#include <Engine.h>
#include <DirectionLight.h>

Scene::Scene(Engine *e) {
    engine = e;

    // Player
    Object *player = createObject<Player>();

    // Rotating camera
    camera = new Camera();
    camera->addShader(engine->shader);
    camera->setTarget(5, 0, 5);

    // dir light
    light = createObject<DirectionLight>();
    light->setWorldLocation(0, 10, 0);

    // terrain
    Object* t = createObject<Terrain>();
    t->setRotation(90, 1, 0, 0);
    t->setScale(10, 10, 1);
    t->setWorldLocation(0, 0, 0);
}

void Scene::render() {
    for (Object *obj : objects) {
        obj->render();
    }
}

void Scene::tick(double delta) {
    glm::vec3 loc(5, 3, 5);
    loc.z += (float) (sin(glfwGetTime() * 0.25) * 10);
    loc.x += (float) (cos(glfwGetTime() * 0.25) * 10);
    camera->setWorldLocation(loc);
    camera->tick();

    glm::vec3 lightPos(5, 2, 5);
    lightPos.z += (float) (sin(glfwGetTime() * 1) * 10);
    lightPos.x += (float) (cos(glfwGetTime() * 1) * 10);
    lightPos.y += (float) (cos(glfwGetTime() * 0.25) * 1);    
    light->setWorldLocation(lightPos);
}

void Scene::resize(int width, int height) {
    camera->updateProjection(width, height);
}