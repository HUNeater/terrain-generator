#include <Camera.h>
#include <glm/gtc/matrix_transform.hpp>

Camera::Camera(GLfloat x, GLfloat y, GLfloat z) : Object(x, y, z) {
    isVisible = false;
    target = glm::vec3(0, 0, 0);
}

void Camera::tick() {
    glm::mat4 viewMatrix;
    viewMatrix = glm::lookAt(worldLocation, target, glm::vec3(0, 1, 0));
    
    for (Shader *s : shaders) {
        uniform(s, "view", viewMatrix);
    }
}

void Camera::addShader(Shader *s) {
    shaders.push_back(s);
    updateProjection(640, 480);
}

void Camera::updateProjection(int width, int height) {
    glm::mat4 projectionMatrix = glm::perspective(glm::radians(45.0f), (float) width / height, 0.1f, 100.0f);
    
    for (Shader *s : shaders) {
        uniform(s, "projection", projectionMatrix);
    }
}

Camera* Camera::setTarget(glm::vec3 t) {
    target = t;
    return this;
}

Camera* Camera::setTarget(GLfloat x, GLfloat y, GLfloat z) {
    target.x = x;
    target.y = y;
    target.z = z;

    return this;    
}