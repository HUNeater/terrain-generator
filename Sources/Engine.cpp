#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <Engine.h>
#include <Terrain.h>
#include <random>
#include <Player.h>
#include <Scene.h>

int main(void) {
    srand(time(NULL));

    try {
        Engine Engine;
        Engine.start();
    } catch (std::runtime_error e) {
        std::cerr << e.what() << std::endl;
    }
    
    return 0;
}

int Engine::start() {
    initGlfw();
    initWindow();
    initGlew();
    initGl();
    initEngine();

    loop();

    glfwTerminate();
    return 0;
}

int Engine::initGlfw() {
    glfwSetErrorCallback(errorHandler);

    if (!glfwInit()) {
        std::cerr << "glfw FAILED" << std::endl;
        glfwTerminate();
        return -1;
    }

    return 0;
}

int Engine::initWindow() {
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    window = glfwCreateWindow(width, height, TITLE, NULL, NULL);
    if (!window) {
        std::cerr << "window FAILED" << std::endl;

        glfwTerminate();
        return -1;
    }

    glfwSetFramebufferSizeCallback(window, resizeHandler);
    glfwSetWindowUserPointer(window, this);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    const GLFWvidmode *vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    glfwSetWindowPos(window,
            (vidMode->width - width) / 2,
            (vidMode->height - height) / 2
    );

    glfwShowWindow(window);
    return 0;
}

int Engine::initGlew() {
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (GLEW_OK != err) {
        fprintf(stderr, "glew FAILED: %s\n", glewGetErrorString(err));
    }

    return 0;
}

int Engine::initGl() {
    glClearColor(0, 0, 0, 1);

    glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, width, height);

    return 0;
}

void Engine::initEngine() {
    const std::string vxCode =
        #include <vertex.glsl>
    ;

    const std::string frCode =
        #include <fragment.glsl>
    ;

    shader = new Shader(vxCode, frCode);
    defaultMaterial = new Material(shader);

    Scene *scene = new Scene(this);
    activeScene = scene;
}

void Engine::loop() {
    double firstTime = glfwGetTime();
    double fpsTimer = 0;
    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();

        double lastTime = glfwGetTime();
        double deltaTime = lastTime - firstTime;
        firstTime = lastTime;
        fpsTimer += deltaTime;

        if (fpsTimer >= 1) {
            std::cout << "fps: " << int(1 / deltaTime) << std::endl;
            fpsTimer = 0;
        }
        
        tick(deltaTime);
        render();

        glfwSwapBuffers(window);
    }
}

void Engine::tick(double delta) {
    activeScene->tick(delta);
}

void Engine::render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    activeScene->render();
}

void Engine::errorHandler(int, const char *err) {
    std::cerr << err << std::endl;
    glfwTerminate();
    std::exit(-1);
}

void Engine::resizeHandler(GLFWwindow *window, int width, int height) {
    glViewport(0, 0, width, height);
    
    Engine *engine = static_cast<Engine*>(glfwGetWindowUserPointer(window));
    Scene *scene = engine->activeScene;
    scene->resize(width, height);
}