#include <Material.h>
#include <iostream>

Material::Material(Shader *s) {
    shader = s;
}

void Material::use() {
    shader->use();

    if (texture) {
        uniform(*shader, "texScale", textureScale);
        glBindTexture(GL_TEXTURE_2D, texture);
    }
}

void Material::setShader(Shader *s) {
    shader = s;
}

void Material::setTextureScale(GLfloat scale) {
    textureScale = scale;
}

void Material::setTexture(Texture *tex) {
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tex->width, tex->height, 0, GL_RGB, GL_UNSIGNED_BYTE, tex->data);
    glGenerateMipmap(GL_TEXTURE_2D);
}

Shader* Material::getShader() {
    return shader;
}

Material::~Material() {

}