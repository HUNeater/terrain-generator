R"(
    #version 450 core
    
    out vec4 FragColor;
    
    in vec3 objectColor;
    in vec3 normalVector;
    in vec3 fragmentPosition;

    uniform vec3 lightColor;
    uniform vec3 lightPosition;

    void main() {
        vec3 norm = normalize(normalVector);
        vec3 lightDirection = normalize(lightPosition - fragmentPosition);

        float diff = max(dot(norm, lightDirection), 0.0);
        vec3 diffuse = diff * lightColor;

        float ambientStrength = 0.1;
        vec3 ambient = ambientStrength * lightColor;

        vec3 result = (ambient + diffuse) * objectColor;
        FragColor = vec4(result, 1.0);
    }
)"