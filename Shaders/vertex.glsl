R"(
    #version 450 core

    layout (location = 0) in vec3 aPos;
    layout (location = 1) in vec3 aColor;
    layout (location = 2) in vec3 aNormal;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;
    uniform float texScale;

    out vec3 objectColor;
    out vec3 normalVector;
    out vec3 fragmentPosition;

    void main() {
        gl_Position = projection * view * model * vec4(aPos, 1.0);
        objectColor = aColor;
        normalVector = aNormal;
        fragmentPosition = vec3(model * vec4(aPos, 1.0));
    }
)"